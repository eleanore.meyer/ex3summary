\section{Wellen in Materie}

\subsection{Maxwell Gleichungen}
Die \emph{elektrische Feldstärke} wird bezeichnet als $\vE$, die davon verschiedene \emph{Flussdichte}
als $\vD$ mit $\vD = \eps_0 \eps_r \vE$. Analog dazu wird zwischen der \emph{magnetischen Feldstärke} $\vH$, sowie der \emph{magnetischen Flussdichte} $\vB = \mu_0 \mu_R \vH$ unterschieden. Die Maxwell-Gleichungen lauten:


\begin{align*}
  \div \vD = \rho &\iff \oiint_{\partial V} \! \vD\cdot \mathrm d \vec A = \iiint_{V}\! \rho \,\mathrm dV \\
  \rot \vE = - \frac{\partial \vB}{\partial t} &\iff \oint_{\partial A} \vE \cdot \mathrm d\vec s
    = - \iint_A \frac{\partial B}{\partial t} \cdot \mathrm d \vec A\\
  \div \vB = 0    &\iff \int_{\partial V} \! \vB \cdot \mathrm d\vec A = 0\\
  \rot \vH = \vec j + \underbrace{\frac{\partial \vD}{\partial t}}_\text{Verschiebungsstr.} &\iff
    \oint_{\partial A} \! \vH \cdot\mathrm d\vec s = \iint_A \!
      \left( \vec j + \frac{\partial \vD}{\partial t} \right) \cdot \mathrm d\vec A
\end{align*}

Im \emph{Vakuum} lässt sich mit $\vec j = 0$, $\eps_r = \mu_r = 0$ und $\rho_0$ die Wellengleichung herleiten:

\begin{align*}
  \Delta \vE &= \eps_0 \mu_0 \frac{\partial^2 \vE}{\partial t^2} \\
  \Delta \vH &= \eps_0 \mu_0 \frac{\partial^2 \vH}{\partial t^2}
\end{align*}

\subsection{Ebene Wellen}
Wellen welche in einer Ebene schwingen und eine Ausbreitungsrichtung $z$ haben heißen \emph{ebene Wellen}.
Die Schwingung wird beschrieben durch:


\begin{align*}
  E_x (zt) &= E_{0,x} \cdot \sin (kz - \omega t + \varphi_x) \\
  E_y (zt) &= E_{0,y} \cdot \sin (kz - \omega t + \varphi_y)
\end{align*}

\subsection{Polarisaton}
Wenn bei einer ebene Welle beide Komponenten in Phase schwingen ist die Welle \emph{linear polarisiert}.
Der $\vB$ Vektor steht hier senkrecht auf dem $\vE$ Vektor mit $\vB = \frac 1 \omega \cdot \left(\vec k \times \vE\right)$.
Bei einer Phasenverschiebung um $\SI{90}{\degree}$ beschreibt $\vE$ einen Kreis.
Deswegen spricht man hier von \emph{zirkulärer Polarisation}.
In allen anderen Fällen ist das Licht \emph{elliptisch polarisiert}.

\subsection{Energie von Wellen und Poynting-Vektor}
Die \emph{Energiedichte} eines elektromagnetischen Feldes beträgt $w = \frac 12 \eps_0 \left(E^2 + c^2B^2\right) = \frac 12\eps_0 E^2$.
Bei linear polarisiertem Licht schwankt die Intensität, so dass die mittlere Intesität $\langle I(t)\rangle = \frac 12 c\eps_0 E^2$ beträgt.

Die Richtung und Betrag des Energieflusses wird im Poynting-Vektor $\poynting = \vE\times\vH$ zusammengefasst.

\subsection{Brechungsindex}
Wenn der Brechungsindex nicht eines Mediums auch von der Wellenlänge abhängig ist, so spricht man von \emph{Dispersion}.
Ein bekanntes Beispiel für \emph{Dispersion} ist zum Beispiel das Aufteilen eines Lichtstrahls in seine Farben mithilfe eines Prismas.
Der Brechungsindex ist i.A. eine komplexe Zahle und kann als $n = n' - i\kappa$ in Real- und Imaginärteil aufgeteilt werden.
Der Imaginärteil beschreibt hier die Abschwächung der Wellenamplitude mit $e^{-\kappa k z}$ bei Durchqueren des Mediums.
Für die Intensität gilt das \emph{Beersche Absorbtionsgesetz}:

\[I (z) = I_0 \cdot e^{-Az} \quad \text{mit } A = 2\kappa k_0\]

Für durchsichtige Medien ist die Absorption sehr gering, daher $n = n'$.

Wenn die Frequenz gegen $\omega_0$ geht steigt zunächst $\kappa$ und damit die Absorption (\emph{normale Dispersion}), um danach wieder abszufallen (\emph{anomale Dispersion}).
Es liegt also genau dann normale Dispersion vor, falls $\nicefrac{dn'}{d\omega} > 0$.

Jede Frequenz hat aufgrund der Dispersion i.A. eine eigene \emph{Phasengeschwindigkeit} $\vph = \frac{c_0}{n' (\lambda)}$.
Die Gruppengeschwindigkeit ergibt sich zu $\vgr = \nicefrac{\partial\omega}{\partial k}$. Genau im Falle normaler Dispersion ist
die Gruppengeschwindigkeit durch die Vakuums-Lichtgeschwindigkeit $c_0$ beschränkt.

\subsection{Transmission und Reflexion}
Das Transmissions- reskepektive Reflexionsvermögen $T$/$R$ ergibt sich aus den Fresnel-Gleichungen.
Aufgrund der Energieerhaltung gilt: $T + R = 1$ (Wir vernachlässigen die Absorbtion).
Im Falle senkrechten Einfalls, d.h. $\alpha = \SI{0}{\degree}$ gilt zudem:

\[R = \left( \frac{n_1 - n_2}{n_1 + n_2} \right)^2\]

\subsection{Brewster-Winkel}
\label{sec:Brewster}
Wenn an einem Medium Licht unter dem \emph{Brewster-Winkel} $\alpha_B$ einfällt, so enthält der reflektierte Strahl keine Komponente welche parallel zur Einfallsebene polarisiert ist.
Der Brewster-Winkel ist gegeben durch:

\[\tan (\alpha_B)  = \frac{n_2}{n_1}\]

\subsection{Phasensprung nach Fresnel-Formeln}
In manchen Fällen tritt bei der Reflexion ein Phasensprung um $\pi$ auf.
Siehe auch \cref{fig:phasensprung}.

\begin{figure}[h]
  \centering
  \begin{tabular}{|c|c|c|}
    \hline
    Reflexion & $E_s$ & $E_p$ \\
    \hline
    An optisch dichterem Medium & $\pi$ & $\begin{cases}0&\alpha<\alpha_B\\\pi&\alpha>\alpha_B\end{cases}$\\
    \hline
    An optisch dünnerem Medium & $0$ & $\begin{cases}\pi&\alpha<\alpha_B\\0&\alpha>\alpha_B\end{cases}$\\
    \hline
  \end{tabular}
  \caption{Phasensprung bei Reflexionen}
  \label{fig:phasensprung}
\end{figure}

\subsection{Totalreflektion}
Beim Übergang eines Lichtstrahls aus einem optisch dichteren Medium $n_1$ in ein optisch dünneres Medium
$n_2$, d.h. $n_1 > n_2$ gilt nach dem Brechungsgestz für den transmittierten Winkel
$\sin(\alpha) = \nicefrac{n_2}{n_1}\cdot \sin (\beta) \leq \nicefrac{n_2}{n_1}$.
D.h. $\alpha$ mit $\sin (\alpha) = \nicefrac{n_2}{n_1}$ ist der Grenzwinkel der Totalreflektion.

\subsection{Doppelbrechung}
\label{sec:Doppelbrechung}
Anisotrope Medien verfügung über einen von der Richtung abhängigen Brechungsindex.
Im folgen betrachten wir unaxiale Kristalle, welche über genau eine \emph{optische Achse} verfügen.
Ein Strahl, welcher sich entlang einer solchen optischen Achse ausbreitet, erfährt unabhängig von der Polarisationsrichtung den selben Brechungsindex.
Zur Berechnung der Brechung wird die sogenannte \emph{Hauptschnittebene} eingefürt, welche durch die optische Achse und den Einfallsvektor aufgespannt wird.
Hier wird ferner zwischen dem \emph{ordentlichen Strahl} (senkrecht zur Hauptschnittebene)
schwingend, sowie dem außerordentlichen Strahl (parallel schwinkend unterschieden).
\todo{Stimmt das?}
Die Brechung beider Strahlen ergibt sich mithilfe des Brechungsgesetz wobei
der Einfallswinkel nun in Bezug auf die
Hauptschnittebene berechnet wird.

\subsection{Malussches Gesetz}
Sei $\vartheta$ die Winkeldifferenz zwischen einem Polfilter und der Polarisation der einfallenden Welle.
Dann gilt für die den Polfilter durchdringende Intensität $I(\vartheta)$:

\[I(\vartheta) = I_0 \cdot \cos^2 (\vartheta)\]

\subsection{Erzeugung polarisierten Lichts}
Der \emph{Polarisationsgrad} ist $\Pi = \frac{I_\parallel - I_\perp}{I_\parallel + I_\perp}$ bezeichnet.

\begin{itemize}
  \item \textbf{Brewster-Winkel}: \cref{sec:Brewster}
  \item \textbf{Dichroismus}: Selektive Absorption einer der beiden orthogonalen Komponenten des E-Felds z.b. durch Kristalle wie Turmalin.
  \item \textbf{Polarisationsfilter/Polaroid-Filter}:
  \item \textbf{Doppelbrechende Polarisatoren}: Z.B. Wollaston Prisma siehe auch \cref{sec:Doppelbrechung}
  \item \textbf{Doppelbrechende Plättchen}: Da jede Polarisationskomponente einen unterschiedliche Brechungsindex und damit auch eine unterschiedliche Ausbreitungsgeschwindigkeit hat, folgt dass sich die Phase \glqq dreht\grqq.
    \begin{enumerate}
      \item \textbf{$\nicefrac \lambda 4$-Plättchen}: Das $\nicefrac\lambda4$ verursacht eine zusätzliche Phasendifferenz von $\Delta\varphi = \SI{90}{\degree}$.
      \item \textbf{$\nicefrac \lambda 2$-Plättchen}: Das $\nicefrac\lambda2$ verursacht eine zusätzliche Phasendifferenz von $\Delta\varphi = \SI{180}{\degree}$.
    \end{enumerate}
\end{itemize}

\subsection{Optische Aktivität}
Optisch aktive Medien drehen bei beliebiger Richtung der Polarisationsebene des einfallenden linear polarisierten Strahls die Polarisationsebene um einen Winkel $\alpha_s \cdot d$.
Hierbei ist der Koeffizient $\alpha_s$ vom Medium abhängig und $d$ beschreibt die Dicke des Mediums.
