# Zusammenfassung Experimentalphysik III WS 19/20 (Prof. Dr. Tauber)

Dieses Repository enthält eine kleine unvollständige Zusammenstellung von Zusammenhängen, welche nützlich für die Klausur sein
könnten. Das PDF kann kompiliert [hier](https://fabian-niklas.de/ex3.pdf) heruntergeladen werden.
